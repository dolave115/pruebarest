# Sistema REST para tabla de usuarios con PHP,JS Y HTML5

_Sistema desarrollado por Diego Olave a modo de practica y material educativo_

## Comenzando 🚀

_Estas instrucciones te permitirán ejecutar el código y dar una idea básica sobre su funcionamiento._

### Pre-requisitos 📋

_Para poder inicializar el sistema se requiere lo siguiente :_

```
1. POSTGRESQL.
2. XAMPP.
3. PHP 8 (Incluido en XAMPP).
4. Apache (Incluido en XAMPP).
5. Visual Studio Code (Editor de Codigo).
```

### Instalación 🔧
```
_Colocar la carpeta en la instalacion de XAMPP C:\xampp\htdocs_

```

## Despliegue 📦

_Copiar la carpeta del proyecto C:\xampp\htdocs, y cambiar la cadena de conexion de la base de datos en el siguiente archivo /config/conexion.php._

## Construido con 🛠️

_Las herramientas utilizadas son las siguientes_

* [PHP](http://www.php.net/) - BackEnd
* [POSTGRESQL](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads) - Base de Datos
* [Visual Studio Code](https://code.visualstudio.com/) - Editor de Codigo
* [JS](https://www.javascript.com/) - FrontEnd
* [Git](https://git-scm.com/) - Manejador de Versiones
* [HTML5](https://html5.org/) - FrontEnd

## Para usar correctamente el servicio rest y llenar la tabla 🛠️
##USANDO UN EDIT:
http://localhost:90/Prueba/config/ws.php?opcion=edit&id=15&nombre=maicol9999&telefono=2222&email=dolave115@gmail.com&cuenta=1018
USANDO UN GET PARA OBTENER EL JSON DE LOS DATOS:
http://localhost:90/Prueba/config/ws.php?opcion=get
USANDO EL POST PARA CREAR UN USUARIO:
http://localhost:90/Prueba/config/ws.php?opcion=post&nombre=maicol880&telefono=2222&email=dolave115@gmail.com&cuenta=1019
##PARA MAYOR CLARIDAD LOS ARCHIVOS RELACIONADOS AL SERVICIO REST ES EL SIGUIENTE:
    WS,CONFIG Y SETTING EN LA CARPETA CONFIG    
    EL CONTROLADOR DE LA API ES api.php Y SU MODELO Api.php
    PATRON DE DATOS ES SINGLETON
## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://gitlab.com/dolave115/pruebarest)

## Autores ✒️

* **Diego Olave** 

---

