<?php
require 'setting.php';
class Connect extends PDO{
    public function __construct(){
        parent::__construct("pgsql:host=".SERVIDOR.";dbname=".DATABASE,USUARIO,PASSWORD);
        $this->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
    }
}
?>