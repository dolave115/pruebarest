<?php
require_once("../config/config.php");
require_once("../models/Api.php");

class Api1{
    #TODO: controlador para obtener los clientes y regresar el json
    public function get_clientes_controller(){

            $api=new Api();   
            $datos=$api->get_clientes_models();
            $data=Array();
            foreach ($datos as $row) {
                # code...
                $sub_array=array();
                $sub_array[]=$row["nombre"];
                $sub_array[]=$row["telefono"];
                $sub_array[]=$row["email"];
                $sub_array[]=$row["cuenta"];
                $data[]=$sub_array;
            }
            

            echo json_encode($data);
                # code...
            
    }
    #TODO: controlador para crear los clientes con los datos solicitados
    public function create_clientes_controller($nombre,$telefono,$email,$cuenta){
        $api=new Api(); 
        $api->create_clientes_models($nombre,$telefono,$email,$cuenta);
    }
    public function edit_clientes_controller($id,$nombre,$telefono,$email,$cuenta){
        $api=new Api(); 
        $api->edit_clientes_models($id,$nombre,$telefono,$email,$cuenta);
    }
}
?>